SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- --------------------------------------------------------
--
-- Структура таблицы `courier_table`
--

CREATE TABLE `courier_table` (
  `id` int(255) NOT NULL,
  `courier_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `courier_table`
--
INSERT INTO `courier_table` (`id`, `courier_name`) VALUES
(0, 'Врубель Михаил Александрович'),
(1, 'Саврасов Алексей Кондратьевич'),
(2, 'Куинджи Архип Иванович'),
(3, 'Крамской Иван Николаевич'),
(4, 'Рублев Андрей Иванов'),
(5, 'Серебрякова Зинаида Евгеньевна'),
(6, 'Айвазовский Иван Константинович'),
(7, 'Серов Валентин Александрович'),
(8, 'Поленов Василий Дмитриевич'),
(9, 'Шакал Марк Захарович');

--
-- Структура таблицы `region_table`
--
CREATE TABLE `region_table` (
  `id` int(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `time_to` int(255) NOT NULL,
  `time_from` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `region_table`
--
INSERT INTO `region_table` (`id`, `region`, `time_to`, `time_from`) VALUES
(0, 'Санкт-Петербург', 48, 48),
(1, 'Уфа', 26, 26),
(2, 'Нижний Новгород', 32, 32),
(3, 'Томск', 8, 8),
(4, 'Кострома', 24, 24),
(5, 'Екатеринбург', 12, 12),
(6, 'Новосибирск', 9,9),
(7, 'Воронеж', 25, 25),
(8, 'Стрежевой', 14, 14),
(9, 'Астрахань', 72, 72);

--
-- Структура таблицы `schedule`
--
CREATE TABLE `schedule` (
  `id` int(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `courier` varchar(255) NOT NULL,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `schedule`
--
INSERT INTO `schedule` (`id`, `id region`, `id courier`, `date_from`, `date_to`) VALUES
(3, '3', '3', '2020-07-30', '2020-07-30'),
(4, '4', '4', '2020-08-01', '2020-08-01'),
(5, '5', '5', '2020-12-24', '2020-12-23'),
(6, '6', '6', '2021-08-20', '2021-08-19'),
(7, '7', '7', '2021-07-18', '2021-07-1'),
(8, '8', '8', '2021-05-08', '2021-05-08'),
(9, '9', '9', '2021-07-03', '2021-07-02'),
(10, '0', '0', '2021-08-29', '2021-08-29'),
(11, '1', '1', '2022-04-18', '2022-04-18'),
(12, '2', '2', '2022-05-22', '2022-06-21');

--
-- Индексы сохранённых таблиц
--

-- Индексы таблицы `courier_table`
--
ALTER TABLE `courier_table`
  ADD KEY `id` (`id`);
--
-- Индексы таблицы `region_table`
--
ALTER TABLE `region_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
--
-- Индексы таблицы `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);
--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `courier_table`
--
ALTER TABLE `courier_table`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;
COMMIT;